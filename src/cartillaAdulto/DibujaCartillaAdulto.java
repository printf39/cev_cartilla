package cartillaAdulto;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import fuentes.Fuentes;


public class DibujaCartillaAdulto {
	
	///
	public static void main(String[] args) {
		DibujaCartillaAdulto inicia = new DibujaCartillaAdulto();
		inicia.generaImagenCartillaAdulto();
	}
	
	///_____________________________________________

	//Generar Cartilla
	public void generaImagenCartillaAdulto() {
		System.out.println("Generando Cartilla Adulto");
		
		BufferedImage imageCartilla = new BufferedImage(2480, 3507, BufferedImage.TYPE_INT_ARGB);
		Graphics2D canvas = imageCartilla.createGraphics() ;
		
		canvas.setBackground(Color.WHITE);
		canvas.clearRect(0, 0, 2480, 3507);
		
		//----Recibe informacion
		String nombre = "Benito Ramos Reyes";
		String fechaNacimiento = "10/10/1010";
		String curp = "NullPointerException";
		String telefono = "5512345678";

		String dosisNeumococica = "1ra";
		String dosisInfluemza = "2ra";
		String dosisHepapitis = "3ra";
		String dosisSR = "4ra";

		String fechaNeumococica = "10 Oct 2030";
		String fechaInfluenza = "10 Oct 2005";
		String fechaHepatitis = "10 Oct 1990";
		String fechaSR = "10 Oct 1950";

		informacionAdulto(nombre, 
		        fechaNacimiento,
		        curp,
		        telefono,
		        dosisNeumococica,
		        dosisInfluemza,
		        dosisHepapitis,
		        dosisSR,
		        fechaNeumococica,
		        fechaInfluenza,
		        fechaHepatitis,
		        fechaSR);
		//----------
		
		
		//Llamada a Metodos Cartilla ------------------------------
		
		//Dibuja Iconos CEV
		pintaImagenesCEV(canvas);
		
		//Dibuja Encabezado CEV
		dibujaInforEncabezadoCEV(canvas);
		
		//Dibuja Texto Cartilla
		dibujaInfoCEV(canvas);
		
		//Dibuja Informacion Adulto
		dibujaInforAdulto(canvas);
		
		//Dibuja Marco Cartilla
		pintaMarco(canvas);
		
		//Dibuja Informacion del Adulto
		dibujaInformacionAdultoCartilla(canvas);
		
		//Dibuja Informacion Vacunas
		pintaInfoVacunas(canvas);
		
//		//Dibuja FOTO
//	    pintaImagenFoto(canvas);
		
		try {
			
			File ruta = new File("C:/Users/tocl/Downloads");
			File fileimagenCartilla = new File(ruta, "cartillaAdultoTest.png");
			
			if(fileimagenCartilla.exists()) {
				fileimagenCartilla.delete();
			}
			
			ImageIO.write(imageCartilla, "png", fileimagenCartilla);
			
			System.out.println(".");
			System.out.println(".");
			System.out.println(".");
			
	        System.out.println("Termino Exitosamente");
	        
		} catch (Exception e) {
			System.err.println("Error--------->"+ e.toString());
			System.err.println("Fallo al generar la Imagen");
		}
		
	}
	//__________________________________________________________________
	
	//Generar PDF Cartilla
	public void generaPDFCartillaAdulto( ) {
		System.out.println("Generando Cartilla Adulto");
		
		//----Recibe informacion
		String nombre = "Benito Ramos Reyes";
		String fechaNacimiento = "10/10/1010";
		String curp = "NullPointerException";
		String telefono = "5512345678";

		String dosisNeumococica = "1ra";
		String dosisInfluemza = "2ra";
		String dosisHepapitis = "3ra";
		String dosisSR = "4ra";

		String fechaNeumococica = "10 Oct 2030";
		String fechaInfluenza = "10 Oct 2005";
		String fechaHepatitis = "10 Oct 1990";
		String fechaSR = "10 Oct 1950";

		informacionAdulto(nombre, 
		        fechaNacimiento,
		        curp,
		        telefono,
		        dosisNeumococica,
		        dosisInfluemza,
		        dosisHepapitis,
		        dosisSR,
		        fechaNeumococica,
		        fechaInfluenza,
		        fechaHepatitis,
		        fechaSR);
		//----------
		
		try {

			Rectangle pageSize = new Rectangle(2480, 3507);
			Document document = new Document(pageSize);
		    
		    PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream("C:/Users/tocl/Downloads/CEV_CartillaAdulto.pdf"));

		    document.open(); 
		    
		    PdfContentByte cb = writer.getDirectContent();
		    
		    Graphics2D graphics2D = cb.createGraphics(2480, 3507);
		    
		    //Llamada a Metodos Cartilla ------------------------------
			
			//Dibuja Iconos CEV
			pintaImagenesCEV(graphics2D);
			
			//Dibuja Encabezado CEV
			dibujaInforEncabezadoCEV(graphics2D);
			
			//Dibuja Texto Cartilla
			dibujaInfoCEV1(graphics2D);
			
			//Dibuja Informacion Adulto
			dibujaInforAdulto(graphics2D);
			
			//Dibuja Marco Cartilla
			pintaMarco(graphics2D);
			
			//Dibuja Informacion del Adulto
			dibujaInformacionAdultoCartilla(graphics2D);
			
			//Dibuja Informacion Vacunas
			pintaInfoVacunas(graphics2D);
			
//			//Dibuja FOTO
//		    pintaImagenFoto(graphics2D);
		    
		    graphics2D.dispose();   
		    document.close();
		    
		    System.out.println(".");
			System.out.println(".");
			System.out.println(".");
			
	        System.out.println("Termino Exitosamente");
			
		} catch (Exception e) {
			
			System.err.println("Error--------->"+ e.toString());
			System.err.println("Fallo al generar el PDF");
			
		}
			
			
		
		
		
	}
	
	
	
	//---------------------------------------------------------------------------------
	//Atributos
	private String nombre; 
	private String fechaNacimiento;
	private String curp;
	private String telefono;
	
	private String dosisNeumococica;
	private String dosisInfluemza;
	private String dosisHepapitis;
	private String dosisSR;
	
	private String fechaNeumococica;
	private String fechaInfluenza; 
	private String fechaHepatitis; 
	private String fechaSR;
	
	//Recibe informacion
	public void informacionAdulto(String nombre,
								String fechaNacimiento,
								String curp,
								String telefono,
								String dosisNeumococica,
								String dosisInfluemza,
								String dosisHepapitis,
								String dosisSR,
								String fechaNeumococica,
								String fechaInfluenza,
								String fechaHepatitis,
								String fechaSR) {

			this.nombre = nombre;
			this.fechaNacimiento = fechaNacimiento;
			this.telefono = telefono;
			this.curp = curp;
			
			this.dosisNeumococica = dosisNeumococica;
			this.dosisInfluemza = dosisInfluemza;
			this.dosisHepapitis = dosisHepapitis;
			this.dosisSR = dosisSR;
			
			this.fechaNeumococica = fechaNeumococica;
			this.fechaInfluenza = fechaInfluenza;
			this.fechaHepatitis = fechaHepatitis;
			this.fechaSR = fechaSR;
	}
	
	//_________________________________________________________________________________
	
	//Informacion de Responsable
	private void dibujaInformacionAdultoCartilla(Graphics2D canvas) {
		Fuentes fuente = new Fuentes();

		//Letra
		canvas.setColor( Color.BLACK);
		canvas.setFont(fuente.fuente(fuente.ROBOTO_REGULAR, 1, 54));
		
		//Nombre
		canvas.drawString(nombre, 1280, 1155 );
		
		//Fecha Nacimiento 
		canvas.drawString(fechaNacimiento, 1280, 1335 );
		
		//CURP
		canvas.drawString(curp, 1280, 1510 );
		
		//Telefono
		canvas.drawString(telefono, 1280, 1690 );
	}
	
	//Informacion de Vacunas
	private void pintaInfoVacunas(Graphics2D canvas) {
		Fuentes fuente = new Fuentes();
		
		//Letra
		canvas.setColor( Color.BLACK);
		canvas.setFont(fuente.fuente(fuente.ROBOTO_MEDIUM, 1, 56));
		
	// NEUMOCOCICA
		//Dosis 
		canvas.drawString(dosisNeumococica, 915, 2130);
		//Fecha Aplicacion
		canvas.drawString(fechaNeumococica, 1690, 2130);
		
	// INFLUENZA
		//Dosis
		canvas.drawString(dosisInfluemza, 915, 2420 );
		//Fecha Aplicacion
		canvas.drawString(fechaInfluenza, 1690, 2420 );
		
	// HEPATITIS B
		//Dosis
		canvas.drawString(dosisHepapitis, 915, 2710 );
		//Fecha Aplicacion
		canvas.drawString(fechaHepatitis, 1690, 2710 );
		
	// SR
		//Dosis
		canvas.drawString(dosisSR, 915, 3000);
		//Fecha Aplicacion
		canvas.drawString(fechaSR, 1690, 3000 );
	}
	
	//_________________________________________________________________________________
	
	
	//Dibuja Fotografia 
	private void pintaImagenFoto(Graphics2D canvas) {
		
		String ruta = "C:/Graphics_Cartilla_Adulto/cartillaAdulto/resource/images";
		
		try {
			
			BufferedImage muestraImagen = ImageIO.read(new File(ruta, "children7.png"));
			//fotografia
			canvas.drawImage(muestraImagen,450, 1100, null);
			
		} catch (IOException e) {
			System.err.println("Error--------->"+ e.toString());
			System.err.println("No se encontro la imagen");
		}
	}
	
	//Dibuja inocos
	private void pintaImagenesCEV(Graphics2D canvas) {
		
		String ruta = "C:/Graphics_Cartilla_Adulto/cartillaAdulto/resource/images";
		
		try {
			
			BufferedImage muestraImagen = ImageIO.read(new File(ruta, "cev_image_6.png"));
			canvas.drawImage(muestraImagen,255, 320, null);
			
			BufferedImage muestraImagenInfo = ImageIO.read(new File(ruta, "cev_Info_5.png"));
			canvas.drawImage(muestraImagenInfo,380, 1030, null);
			
		} catch (IOException e) {
			System.err.println("Error--------->"+ e.toString());
			System.err.println("No se encontro la imagen");
		}
	}
		
	//Marco Encabezado
	private void dibujaInforEncabezadoCEV(Graphics2D canvas) {
		
		String colorCev = "#5D9A42";
		
		canvas.setColor(Color.decode(colorCev));
		canvas.setStroke(new BasicStroke(16));
		RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(1600, 331, 480, 660, 50, 50);
        canvas.draw(roundedRectangle);
		
        canvas.setBackground(Color.decode(colorCev));
		canvas.clearRect(1066, 338, 1022, 666);
		
		canvas.setBackground(Color.decode(colorCev));
		canvas.clearRect(1066, 328, 1010, 676);
		
		

	}
		
	
	
	//Infor Encabezado	
	private void dibujaInfoCEV(Graphics2D drawer) { 
		
		Fuentes fuente = new Fuentes();
		
		String cev1 = "CARTILLA";
		String cev2 = "ELECTR�NICA";
		String cev3 = "DE VACUNACI�N";
		
		//Letra
		drawer.setColor( Color.WHITE);
		drawer.setFont(fuente.fuente(fuente.ROBOTO_CONDENSED_BOLD, 1, 115));
		
		//CARTILLA
		drawer.drawString(cev1, 1190, 570 );
		drawer.drawString(cev2, 1190, 722);
		drawer.drawString(cev3, 1190, 870);
	}
	
	//Infor Encabezado	
	private void dibujaInfoCEV1(Graphics2D drawer) { 
		
		Fuentes fuente = new Fuentes();
		
		String cev1 = "CARTILLA";
		String cev2 = "ELECTR�NICA";
		String cev3 = "DE VACUNACI�N";
		
		//Letra
		drawer.setColor( Color.WHITE);
		drawer.setFont(fuente.fuente(fuente.ROBOTO_CONDENSED_BOLD, 1, 115));
		
		//CARTILLA
		drawer.drawString(cev1, 1190, 570 );
		drawer.drawString(cev2, 1190, 722);
		drawer.drawString(cev3, 1190, 870);
	}
		
	//Datos Usuario
	private void dibujaInforAdulto(Graphics2D drawer) {

		Fuentes fuente = new Fuentes();
		
		String InfoNombre   = "Nombre:";
		String InfoFechaNac = "Fecha de Nacimiento:";
		String InfoCURP     = "CURP:";
		String InfoTelefono = "Tel�fono:";  
		
		//Letra
		drawer.setColor( Color.GRAY);
		drawer.setFont(fuente.fuente(fuente.ROBOTO_REGULAR, 1, 57));
		
		//Nombre
		drawer.drawString(InfoNombre, 1280, 1087);
		
		//Fecha Nacimiento 
		drawer.drawString(InfoFechaNac, 1280, 1265);
		
		//CURP
		drawer.drawString(InfoCURP, 1280, 1444 );
		
		//Telefono
		drawer.drawString(InfoTelefono, 1280, 1625);
	}
	
	//Marco Imagen
	private void pintaMarco(Graphics2D canvas) {
		String colorMarco = "#454545";
		
		canvas.setColor(Color.decode(colorMarco));
		canvas.setStroke(new BasicStroke(5));
		RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(365, 325, 1725, 2790, 50, 50);
        canvas.draw(roundedRectangle);
		
        //Lineas Tabla
        dibujaTabla(canvas);
	}
	
	//Dibuja Tabla Vacunas
	private void dibujaTabla(Graphics2D canvas) {
		
		String colorCev = "#5D9A42";
		String colorColumna = "#E7E8E6";
		String vacunaSR = "#F69458";
		String lineasTabla = "#B2BABB";
		
		//Color VAcuna
		canvas.setColor(Color.decode(vacunaSR));
		canvas.setStroke(new BasicStroke(16));
		RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(376, 2750,300, 357, 50, 50);
        canvas.draw(roundedRectangle);
		
        //cubrir el color de la vacuna
        canvas.setBackground(Color.decode(colorColumna));
		canvas.clearRect(384,2850, 389,265);
        
		//Columna Dos
		canvas.setBackground(Color.decode(colorColumna));
		canvas.clearRect(1172,1970, 463,1143);
		
        //Columna Uno
		canvas.setBackground(Color.decode(colorColumna));
		canvas.clearRect(368,1970, 405,888);
		
		//Encabezado
		canvas.setBackground(Color.decode(colorCev));
		canvas.clearRect(368,1760, 1720,210);
	
		//Lineas Encabezado Cartilla
		canvas.setColor(Color.WHITE);
		canvas.setStroke(new BasicStroke(5));
		
		//parametro 1,3 
		canvas.drawLine(772, 1790, 772, 1935);
		canvas.drawLine(1170, 1790, 1170, 1935);
		canvas.drawLine(1640, 1790, 1640, 1935);
		
		
		//Lineas Cartilla
		canvas.setColor(Color.decode(lineasTabla));
		canvas.setStroke(new BasicStroke(5));
		canvas.drawLine(370, 1970, 2085, 1970);
		canvas.drawLine(370, 2255, 2085, 2255);
		canvas.drawLine(370, 2540, 2085, 2540);
		canvas.drawLine(370, 2845, 2085, 2845);
				
		//Dibuja Rectangulos Vacunas
		pintaColoresVacunas(canvas);

		//Dibuja Informacion Estatica Encabezado
		pintaInfoEncabezado(canvas);

		//Dibuja Informacion Estatica de las Vacunas
		pintaInfoVacunasDatos(canvas);		
	}
	
	//Colores Vacunas
	private void pintaColoresVacunas(Graphics2D canvas) {

		String vacunaNeumococica = "#67C9F1";
		String vacunaInfluenza = "#F39BC2";
		String vacunaHepatitisB = "#F08D0E";
		
		//Color Neumococica 
		canvas.setBackground(Color.decode(vacunaNeumococica));
		canvas.clearRect(366,1984, 15,255);
		
		//Color Influenza
		canvas.setBackground(Color.decode(vacunaInfluenza));
		canvas.clearRect(366,2272, 15,255);

		//Color Hepatitis
		canvas.setBackground(Color.decode(vacunaHepatitisB));
		canvas.clearRect(366,2558, 15,275);
	}
	
	//Encabezado Tabla
	private void pintaInfoEncabezado(Graphics2D canvas) {
		
		Fuentes fuente = new Fuentes();
		
		String vacuna = "Vacuna"; 
		String dosis = "Dosis";
		String frec = "Frecuencia de";
		String aplicacion = "aplicaci�n";
		String fech1 = "Fecha de";
		
		//Letra
		canvas.setColor( Color.WHITE);
		canvas.setFont(fuente.fuente(fuente.ROBOTO_MEDIUM, 1, 54));
		
		canvas.drawString(vacuna, 465, 1890);
		canvas.drawString(dosis, 895, 1890);
		
		canvas.drawString(frec, 1215, 1857);
		canvas.drawString(aplicacion, 1265, 1920);
		
		canvas.drawString(fech1, 1743, 1857);
		canvas.drawString(aplicacion, 1730, 1920);
	}
	
	//Dibuja informacion Vacunas
	private void pintaInfoVacunasDatos(Graphics2D canvas) {
		
		Fuentes fuente = new Fuentes();
		
		String  neumo1 = "Neumoc�cica";
		String  neumo2 = "conjugada";
		String  influenza = "Influenza";
		String  hepatitis = "Hepatitis B";
		String  sr = "SR";
		String anual = "Anual";

	//Letra
		canvas.setColor( Color.BLACK);
		canvas.setFont(fuente.fuente(fuente.ROBOTO_MEDIUM, 1, 52));
		
	// NEUMOCOCICA
		canvas.drawString(neumo1, 400, 2112);
		canvas.drawString(neumo2, 440, 2175);
		canvas.drawString(anual, 1325, 2130);
		
	// INFLUENZA
		canvas.drawString(influenza, 450, 2420 );
		canvas.drawString(anual, 1325, 2420 );
		
	// HEPATITIS B
		canvas.drawString(hepatitis, 430, 2710 );
		canvas.drawString(anual, 1325, 2710 );
		
	// SR
		canvas.drawString(sr, 535, 3000);
		canvas.drawString(anual, 1340, 3000 );
	}
}
