package cartillaAdulto;

import javax.swing.JOptionPane;

public class CartillaAdultoMain {
	
	public static void main(String[] args) {
		CartillaAdultoMain inicia = new CartillaAdultoMain();
		inicia.generar();
	}
	
	public void generar() {
		
		DibujaCartillaAdulto dibujaCartilla = new DibujaCartillaAdulto();
		
		try {
			
			int entrada = Integer.parseInt(JOptionPane.showInputDialog("1. Genera PDF"
					+ "\n2. Genera Imagen"));
			
			switch (entrada) {
			case 1:
				
				dibujaCartilla.generaPDFCartillaAdulto();
				
				break;
			case 2:
				
				dibujaCartilla.generaImagenCartillaAdulto();
				
				break;
			default:
				System.err.print("Opci�n no encontrada");
				break;
			}
			
		} catch (NumberFormatException e) {
			System.err.println(e + " la entrada no es Valida");
		}
	}
	
}
