package fuentes;

import java.awt.Font;
import java.io.InputStream;

public class Fuentes {
	
	private Font font = null;
	
    public String ROBOTO_MEDIUM = "medium.ttf";
    public String ROBOTO_REGULAR = "regular.ttf";
    public String ROBOTO_CONDENSED_BOLD= "roboto_condensed_bold.ttf";
    
//     //Estilo Fuente
//     * 1- Plano
//     * 2- Negrita 
//     * 3- Cursiva

    public Font fuente( String nombrefuente, int estiloFuente, float tamanoFuente) {
    	
         try {

            InputStream tipofuente =  getClass().getResourceAsStream(nombrefuente);
            font = Font.createFont(Font.TRUETYPE_FONT, tipofuente);
            
        } catch (Exception ex) {
            System.err.println(nombrefuente +"-->  no se encontro la fuente");
        }
         
        Font retornaFuente = font.deriveFont(estiloFuente, tamanoFuente);
        
        return retornaFuente;
    }
    
}
